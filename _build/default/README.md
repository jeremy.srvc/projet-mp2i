![MP2IQUEST Logo](/assets/banner.png)
# MP2IQUEST - Projet MP2I - 2023

Créé dans le but de répondre aux attentes d'un projet de programmation en langage OCaml, **MP2IQUEST** est un jeu graphique utilisant la bibliothèque `Raylib`

## Description du projet

**MP2IQUEST** est un clicker avec une interface s'apparentant à celle d'un jeu téléphone. 
Sur celui-ci, vous retrouverez les fonctionnalités emblématiques du mode de jeu clicker ainsi que quelques ajouts entièrement développés par nos soins. 
Du magasin aux succès en passant par des bonus aléatoires, tout est mis en oeuvre pour que l'expérience de jeu soit la plus longue et divertissante possible. 

## Déroulé du développement

 - Brainstorming et mise en commun de nos idées pour le projet
 - Premiers pas avec la bibliothèque utilisée afin d'en découvrir les fondamentaux
 - Création de la fonction principale du jeu permettant d'incrémenter un compteur à chaque clic
 - Création du menu principal du jeu
 - Mise en place de la D.A (Direction Artistique) du projet
 - Création des types principaux utilisés pour stocker toutes les informations liées à l'avancement du jeu
 - Ajout de nombreuses textures sur les fonctions déjà présentes et d'un support plus adapté pour leur implémentation
 - Développement d'un Menu Echap afin de gérer plus proprement la fermeture du jeu
 - Ajout d'un système de progression avec une barre d'expérience
 - Ajout d'un système de vie avec barre dans le HUD afin d'anticiper de futurs ajouts
 - Changement du "bouton" principal de clic pour le rendre évolutif selon la progression de chaque niveau
 - Développement d'un système de bonus aléatoires afin de rendre plus dynamique les phases de clic
 - Ajout des différents boutons sur l'écran afin d'accéder au magasin, à l'inventaire, aux succès et aux technologies (évoquées ultérieurement)
 - Intégration des succès, du menu aux fonctions de vérification
 - Intégration du magasin et du menu qui en découle
 - Choix des fonctions d'évolution de l'expérience requise pour passer de niveau en niveau et des prix du magasin
 - Ajout de quelques textures en plus dont les notifications de passage de niveau et d'obtention de succès
 - Intégration des technologies sous forme d'arbre de compétences dans un menu propre à cet ajout
 - Ajout d'un support musical au jeu
 - Résolution de bugs 
 - Tests du jeu

## Description des fonctionnalités principales

### 1. Clicker

Le système de clicker permet d'effectuer une action à chaque clic effectué par l'utilisateur. 

Dans notre cas, cela incrémente un compteur de points en prenant en compte les améliorations débloquées par le joueur et le multiplicateur acquis.

### 2. Head Up Display (HUD)

Le HUD est l'affichage présent en haut à gauche de l'écran de jeu. 

On y retrouve la vie du joueur *(finalement inutilisée)*, sa progression dans le niveau actuel, le montant de points non-dépensés et son avatar.

### 3. Menu Echap

Le menu echap permet de faciliter la fermeture du jeu avec un bouton directement présent sur l'écran de jeu mais aussi d'en gérer les paramètres. 

Les crédits du jeu sont aussi disponibles dans ce menu.

### 4. Magasin

Le magasin permet au joueur de dépenser les points obtenus afin d'en gagner encore plus.

Trois produits sont à sa disposition : 
 - L'amélioration du nombre de points gagnés par clic
 - L'amélioration du nombre de points gagnés par seconde
 - L'amélioration du multiplicateur global qui participe à l'augmentation des points gagnés par seconde et par clic

Il est possible d'acheter ces améliorations par 1 unité, 10, 100 ou 1000. 

Le prix affiché est celui d'une unité et sous l'icone du produit se trouve le stade d'évolution actuel.

### 5. Inventaire

Anticipé afin d'ajouter d'éventuelles fonctionnalités au jeu au même titre que la barre de vie.

L'avantage du mode de jeu clicker étant de pouvoir partir dans de nombreuses directions variées.

### 6. Technologies

À la manière d'un arbre de compétence dans un RPG, les technologies permettent au joueur de personnaliser son expérience en ajoutant des améliorations à son aventure.

Décomposées en 3 stades accessibles avec la barre présente sur la gauche du menu, les technologies peuvent être achetées dans un ordre précis afin d'obtenir des améliorations à des prix plus avantageux que le magasin.

### 7. Succès

Les succès permettent de donner un but au jeu. 

Séparés en 5 catégories ils permettent au joueur de balayer les principales fonctionnalités du jeu et d'y passer un bon bout de temps !

### 8. Bonus

Les bonus apparaissent aléatoirement sur l'écran de jeu et permettent de gagner un multiplicateur de points par clic. 

Attention, il faut être rapide car les bonus ne sont cliquables pendant seulement quelques secondes.

### 9. Niveaux & Progression

Afin d'ajouter un fil conducteur à l'expérience de jeu, un système de progression est disponible.

Celui-ci influence l'illustration de la météorite à cliquer.

L'expérience requise afin de passer au niveau supérieure est calculée selon la fonction mathématique suivante :

```ocaml
8 * (current_level³ - (current_level - 1)³)
```

## Description du code 

Dans cette partie, nous analyserons les différents types et fonction afin d'en voir leur utilité dans le jeu.

Les différentes fonctions sont regroupées dans plusieurs sections dans le fichier `main.ml` délimitées par des titres en ASCII.

### Engine

```ocaml
type game_area;
```
Ce type permet de gérer les différentes "fenêtres" du jeu.

En fonction du champ choisi, les fonctions de jeu s'adaptent afin d'afficher et agir de manière adéquate.

```ocaml
type success;
```
Ce type permet de gérer les différents type de succès disponibles et effectuer les tests de complétion en lien avec le type de succès.

```ocaml
type game_stats;
```
Ce type est le type principal du jeu, il permet de gérer toutes les fonctionnalités et de garder un suivi de l'état d'avancement du joueur dans le jeu.

```ocaml
type game_assets;
```
Ce type contient tous les assets du jeu afin de gérer toute la partie graphique avec Raylib.

```ocaml
type game_textures;
```
Ce type va de paire avec le précédent et permet lui aussi de gérer la partie graphique avec Raylib.

```ocaml
fun setup : unit -> unit;
```
Cette fonction permet d'initialiser la fenêtre graphique du jeu utilisée par Raylib.

### Button

```ocaml
type button;
```
Ce type permet la création de boutons selon un modèle précis afin de mieux gérer leur implémentation par les fonctions qui vont suivre.

```ocaml
fun shop_price : int -> game_stats -> float;
```
Cette fonction permet de calculer le prix d'achat d'un produit du shop selon la fonction croissante de multiplicateur de prix choisie. 

Afin de rendre le jeu plus intéressant et complexe, les prix augmentent au fur et à mesure qu'un produit est acheté.

```ocaml
fun press_button : button -> game_stats -> unit;
```
Cette fonction permet de gérer les actions à effectuer suivant le bouton qui a été cliqué.

En effet, à chaque clic sur un bouton, cette fonction est appelé et permet d'agir et/ou rediriger l'exécution vers la fonction adéquate.

```ocaml
fun in_button : button -> int -> int -> bool;
```
Cette fonction permet de tester si un clic est effectuer dans le rectangle délimité par un bouton.

### HUD

```ocaml
fun update_health : game_stats -> game_textures -> unit;
```
Cette fonction met à jour la barre de vie présente dans le HUD à chaque frame. 

```ocaml
fun update_exp : game_stats -> game_textures -> unit;
```
Cette fonction met à jour la barre de progression présente dans le HUD à chaque frame.

```ocaml
fun update_HUD : game_stats -> game_textures -> unit;
```
Cette fonction met à jour le HUD (barre de vie et de progression) à chaque frame.

```ocaml
fun draw_HUD : game_textures -> unit;
```
Cette fonction affiche le fond de l'HUD et l'avatar.

### Achievements

```ocaml
type achievement;
```
Cette fonction permet la création de succès selon un modèle précis afin de mieux gérer leur implémentation dans les fonctions qui suivent.

```ocaml
fun update_achievements : game_stats -> unit;
```
Cette fonction permet de vérifier à chaque frame l'état des succès et de compléter ceux qui doivent l'être.

En cas de complétion, cela déclanche l'affichage d'une notification sur l'écran de jeu. 

Pour ce faire, nous parcourons un tableau contenant tous les succès et regardons uniquement ceux qui ne sont pas encore complétés.

### Playing

```ocaml
fun event_button : game_stats -> unit;
```
Cette fonction génère aléatoirement l'apparition d'un bonus. 

Elle gère aussi la suppression d'un bonus non-récupérer en se basant sur le temps écoulé depuis sa précédente apparition.

```ocaml
fun update_requ_exp : game_stats -> unit;
```
Cette fonction met à jour l'expérience requise en fonction du niveau atteint.

```ocaml
fun click_clicker : int -> int -> game_stats -> unit;
```
Cette fonction est appelé à chaque clic dans la zone de jeu et vérifie si le clic a eu lieu sur un bouton ou non.

En fonction elle redirige l'exécution vers la gestion des boutons ou l'incrémentation du compteur de points.

```ocaml
fun draw_pts : game_stats -> unit;
```
Cette fonction permet d'afficher les points actuellement non dépensés dans le HUD.

```ocaml
fun draw_game_area : game_stats -> game_textures -> unit;
```
Cette fonction permet de changer l'affichage des assets selon l'étape de jeu dans laquelle le joueur se trouve.

Dépendant d'un élément de type `game_area` la fenêtre est modifiée afin de correspondre à l'utilisation attendue.

La fonction étant longue, voici son fonctionnement global :
 - Un `match` permet de rediriger l'exécution en fonction de la zone de jeu
 - Si on se trouve dans `Clicker` on regarde d'abord la progression du joueur pour afficher l'étape de la météorite qui y est reliée. Ensuite, si un bonus est en cours, on l'affiche. De même, si un succès a été passé récemment, on affiche la notification.
 - Si on se trouve dans `Shop` on affiche correctement les différents prix, stades actuels et titres adéquats.
 - Si on se trouve dans `Achievements` on affiche le bon titre de succès en fonction du dernier complété de la série et la progression dans le succès actuellement affiché.
 - Si on se trouve dans `Tech` on affiche la technologie achetable à l'étape actuelle et toutes les autres avec un cadenas pour signaler qu'elles sont pour le moment vérouillées.
 - Si on se trouve dans `Level_Up` on affiche l'image de passage de niveau et on crée le bouton pour continuer à jouer.


### Init

```ocaml
fun game_init : unit -> game_stats;
```
Cette fonction initialise l'avancement du jeu selon les valeurs de départ.

```ocaml
fun game_assets_init : unit -> game_assets;
```
Cette fonction initialise les différents assets utilisés par Raylib.

Cela permet de ne charger le fichier sur la VRam qu'une seule fois pour toute l'exécution. 

```ocaml
fun game_textures_init : game_assets -> game_textures;
```
Cette fonction initialise les différentes textures affichées par Raylib lors de l'exécution du jeu à partir des assets précédemment chargés en VRam.

### Core 

```ocaml
fun loop : game_stats -> game_textures -> unit;
```
Cette fonction est le coeur du jeu. 

Récursive, elle permet d'actualiser le jeu à chaque frame et de son exécution découle l'exécution de toutes les autres fonctions. 

Elle ne s'arrête que lorsque le bouton `Quitter` est pressé dans le menu echap.

C'est par ailleurs elle qui contrôle l'affichage de ce menu echap grâce aux `placeholder` de Raylib.

```ocaml
fun _ : unit -> unit;
```
Cette fonction se lance lors de l'exécution du code, elle initialise les différentes variables primaires comme `game_status` et `game_textures` et lance la première récursion de la fonction `loop`.

## Crédits

Jeu réalisé par **Jeremy SERIEVIC** et **Raphaël FULCHERI** élèves de MP2I.

L'ensemble des textures/images utilisées dans MP2IQUEST ont été modifiée/créées/adaptées par nos soins.

Certaines proviennents avant modification de :
 - [itch.io](https://itch.io/)
 - [District Games](https://www.districtgames.fr/)

Bon jeu sur MP2IQUEST !