let width = 720
let height = 920

(*
 ________  __    __   ______   ______  __    __  ________ 
|        \|  \  |  \ /      \ |      \|  \  |  \|        \
| $$$$$$$$| $$\ | $$|  $$$$$$\ \$$$$$$| $$\ | $$| $$$$$$$$
| $$__    | $$$\| $$| $$ __\$$  | $$  | $$$\| $$| $$__    
| $$  \   | $$$$\ $$| $$|    \  | $$  | $$$$\ $$| $$  \   
| $$$$$   | $$\$$ $$| $$ \$$$$  | $$  | $$\$$ $$| $$$$$   
| $$_____ | $$ \$$$$| $$__| $$ _| $$_ | $$ \$$$$| $$_____ 
| $$     \| $$  \$$$ \$$    $$|   $$ \| $$  \$$$| $$     \
 \$$$$$$$$ \$$   \$$  \$$$$$$  \$$$$$$ \$$   \$$ \$$$$$$$$
*)

(* TYPES *)

type game_area = 
  | Menu
  | Clicker
  | Escape
  | Settings
  | Credits
  | Shop
  | Inventory
  | Tech
  | Achievements
  | Level_Up

type success =
  | Click
  | Points
  | PPS 
  | Achievements
  | Events 

type game_stats = {
  mutable pts_per_click : float; (* Current number of points earned per click without multiplicator *)
  mutable pts_per_sec : float; (* Current number of points earned per second without multiplicator *)
  mutable pts : float; (* Current number of points *)
  mutable clicks_count : int; (* Total number of clicks  done since the beginning of the game *)
  mutable total_pts : float; (* Total number of points earned since the beginning of the game *)
  mutable prestige : int; (* Current prestige level of technologies *)
  mutable global_mult : float; (* Current global multiplicator affecting points earned by clicking or idling *)
  mutable click_mult : float; (* Current multiplicator affecting points earned by clicking *)
  mutable pps_mult : float; (* Current multiplicator affecting points earned by idling *)
  mutable click_count_mult : int; (* Current multiplicator affecting clicks counted in clicks_count *)
  mutable achievements_count : int; (* Number of achievement accomplished *)
  mutable time_spent : int; (* Time spent in the game in seconds *)
  mutable area : game_area; (* Current location of the player *)
  mutable level : int; (* Current progression level *)
  mutable exp : float; (* Current amount of experience *)
  mutable requ_exp : float; (* Total amount of experience needed to level up *)
  mutable life : float; (* Current amount for health point *)
  mutable max_life : float; (* Current maximum amount of health point *)
  mutable defence : float; (* Current amount for defence point *)
  mutable attack : float; (* Current amount for damage deal per click *)
  mutable event_start : float; (*Time of the start of an event, (= 0. when there is no event)*)
  mutable events_clicked : int; (* Current amount of events clicked since the beginning of the game *)
  mutable success_display_start : float; (*Time of the start of the display of a succes*)
  mutable last_success : success; (*Last success achieved*)
  mutable tech_navigate : int list; (* Table of the different postion in the Tech area *)
  mutable tech_position : int; (* Current position in tech_navigate *)
  mutable tech_level : int array; (* Current level for each Tech category (0 to 5) *)
  mutable shop_click : int; (* Current number of point per click upgrades bought in the shop *)
  mutable shop_pps : int; (* Current number of point per second upgrades bought in the shop *)
  mutable shop_mult : int; (* Current number of multiplicator upgrades bought in the shop *)
  mutable music_volume : float; (*Current music volume*)
} 

type game_assets = {
  background : Raylib.Image.t;
  character : Raylib.Image.t;
  coins_bar : Raylib.Image.t;
  avatar_background : Raylib.Image.t;
  coins : Raylib.Image.t;
  heart : Raylib.Image.t;
  join : Raylib.Image.t;
  level : Raylib.Image.t;
  life_bar : Raylib.Image.t;
  life_progress : Raylib.Image.t;
  life_progress0 : Raylib.Image.t;
  life_progress1 : Raylib.Image.t;
  main_title : Raylib.Image.t;
  xp_bar : Raylib.Image.t;
  escape : Raylib.Image.t;
  xp_progress : Raylib.Image.t;
  xp_progress0 : Raylib.Image.t;
  xp_progress1 : Raylib.Image.t;
  button_settings : Raylib.Image.t;
  explo1 : Raylib.Image.t;
  explo2 : Raylib.Image.t;
  explo3 : Raylib.Image.t;
  explo4 : Raylib.Image.t;
  explo5 : Raylib.Image.t;
  explo6 : Raylib.Image.t;
  button_inventory : Raylib.Image.t;
  button_achievements : Raylib.Image.t;
  button_tech : Raylib.Image.t;
  button_shop : Raylib.Image.t;
  achievements_menu : Raylib.Image.t;
  button_back : Raylib.Image.t;
  bonus : Raylib.Image.t;
  shop_menu : Raylib.Image.t;
  success_display : Raylib.Image.t;
  success_points : Raylib.Image.t;
  success_pps : Raylib.Image.t;
  success_event : Raylib.Image.t;
  level_up_card : Raylib.Image.t;
  tech1 : Raylib.Image.t;
  tech1_2 : Raylib.Image.t;
  tech1_3 : Raylib.Image.t;
  tech1_4 : Raylib.Image.t;
  tech1_5 : Raylib.Image.t;
  tech2 : Raylib.Image.t;
  tech2_1 : Raylib.Image.t;
  tech2_2 : Raylib.Image.t;
  tech2_3 : Raylib.Image.t;
  tech2_4 : Raylib.Image.t;
  tech2_5 : Raylib.Image.t;
  tech3 : Raylib.Image.t;
  tech3_1 : Raylib.Image.t;
  tech3_2 : Raylib.Image.t;
  tech3_3 : Raylib.Image.t;
  tech3_4 : Raylib.Image.t;
  tech3_5 : Raylib.Image.t;
  tech_buy_sign : Raylib.Image.t;
  tech_locked : Raylib.Image.t;
  credits_menu : Raylib.Image.t;
  settings_menu : Raylib.Image.t;
}

type game_textures = {
  background : Raylib.Texture.t;
  character : Raylib.Texture.t;
  coins_bar : Raylib.Texture.t;
  avatar_background : Raylib.Texture.t;
  coins : Raylib.Texture.t;
  heart : Raylib.Texture.t;
  join : Raylib.Texture.t;
  level : Raylib.Texture.t;
  life_bar : Raylib.Texture.t;
  life_progress : Raylib.Texture.t;
  life_progress0 : Raylib.Texture.t;
  life_progress1 : Raylib.Texture.t;
  main_title : Raylib.Texture.t;
  xp_bar : Raylib.Texture.t;
  escape : Raylib.Texture.t;
  xp_progress : Raylib.Texture.t;
  xp_progress0 : Raylib.Texture.t;
  xp_progress1 : Raylib.Texture.t;
  button_settings : Raylib.Texture.t;
  explo1 : Raylib.Texture.t;
  explo2 : Raylib.Texture.t;
  explo3 : Raylib.Texture.t;
  explo4 : Raylib.Texture.t;
  explo5 : Raylib.Texture.t;
  explo6 : Raylib.Texture.t;
  button_inventory : Raylib.Texture.t;
  button_achievements : Raylib.Texture.t;
  button_tech : Raylib.Texture.t;
  button_shop : Raylib.Texture.t;
  achievements_menu : Raylib.Texture.t;
  button_back : Raylib.Texture.t;
  bonus : Raylib.Texture.t;
  shop_menu : Raylib.Texture.t;
  success_display : Raylib.Texture.t;
  success_points : Raylib.Texture.t;
  success_pps : Raylib.Texture.t;
  success_event : Raylib.Texture.t;
  level_up_card : Raylib.Texture.t;
  tech1 : Raylib.Texture.t;
  tech2 : Raylib.Texture.t;
  tech3 : Raylib.Texture.t;
  tech_buy_sign : Raylib.Texture.t;
  tech_locked : Raylib.Texture.t;
  tech_advancement : Raylib.Texture.t array;
  credits_menu : Raylib.Texture.t;
  settings_menu : Raylib.Texture.t;
}

type game_music = {
  background : Raylib.Music.t;
  level_up : Raylib.Music.t;
}

(* FUNCTIONS *)

let setup () = 
  Raylib.init_window width height "MP2I QUEST - 0.0.1";
  Raylib.init_audio_device ();
  Raylib.set_master_volume 0.5;
  Raylib.set_window_icon (Raylib.load_image "assets/icon.png");
  Raylib.set_target_fps 30
  
(*_____________________________________________*)

(*
 _______   __    __  ________  ________   ______   __    __ 
|       \ |  \  |  \|        \|        \ /      \ |  \  |  \
| $$$$$$$\| $$  | $$ \$$$$$$$$ \$$$$$$$$|  $$$$$$\| $$\ | $$
| $$__/ $$| $$  | $$   | $$      | $$   | $$  | $$| $$$\| $$
| $$    $$| $$  | $$   | $$      | $$   | $$  | $$| $$$$\ $$
| $$$$$$$\| $$  | $$   | $$      | $$   | $$  | $$| $$\$$ $$
| $$__/ $$| $$__/ $$   | $$      | $$   | $$__/ $$| $$ \$$$$
| $$    $$ \$$    $$   | $$      | $$    \$$    $$| $$  \$$$
 \$$$$$$$   \$$$$$$     \$$       \$$     \$$$$$$  \$$   \$$
*)

(* TYPE *)

type button = {
  mutable x : int; (* Coordinate of the top left corner of the button *)
  mutable y : int; (* Coordinate of the top left corner of the button *)
  mutable width : int; (* Width of the rectangle covered by the button *)
  mutable height : int; (* Height of the rectangle covered by the button *)
}
(* ITEMS *)

let main_button_join = {x = 175; y = 155; width = 216; height = 80}
let credits_button_resume = {x = 225; y = 604; width = 300; height = 51}
let settings_button_resume = {x = 225; y = 604; width = 300; height = 51}
let settings_button_minus = {x = 416; y = 353; width = 42; height = 42}
let settings_button_plus = {x = 547; y = 353; width = 42; height = 42}
let clicker_button_settings = {x = 675; y = 5; width = 40; height = 40}
let clicker_button_shop = {x = 5; y = 835; width = 170; height = 80}
let clicker_button_inventory = {x = 185; y = 841; width = 170; height = 74}
let clicker_button_tech = {x = 365; y = 852; width = 170; height = 68}
let clicker_button_achievements = {x = 545; y = 841; width = 170; height = 74}
let clicker_button_event = {x = 0; y= 0; width = 0; height = 0}
let clicker_entire_area = {x = 0; y = 0; width = 720; height = 920}
let escape_resume = {x = 225; y = 377; width = 300; height = 51}
let escape_settings = {x = 225; y = 459; width = 300; height = 51}
let escape_credits = {x = 225; y = 543; width = 300; height = 51}
let escape_exit = {x = 225; y = 628; width = 300; height = 51}
let shop_button_back = {x = 520; y = 274; width = 44; height = 44}
let shop_button1_1 = {x = 292; y = 407; width = 37; height = 22}
let shop_button1_10 = {x = 336; y = 407; width = 47; height = 22}
let shop_button1_100 = {x = 390; y = 407; width = 57; height = 22}
let shop_button1_1000 = {x = 454; y = 407; width = 67; height = 22}
let shop_button2_1 = {x = 292; y = 502; width = 37; height = 22}
let shop_button2_10 = {x = 336; y = 502; width = 47; height = 22}
let shop_button2_100 = {x = 390; y = 502; width = 57; height = 22}
let shop_button2_1000 = {x = 454; y = 502; width = 67; height = 22}
let shop_button3_1 = {x = 292; y = 597; width = 37; height = 22}
let shop_button3_10 = {x = 336; y = 597; width = 47; height = 22}
let shop_button3_100 = {x = 390; y = 597; width = 57; height = 22}
let shop_button3_1000 = {x = 454; y = 597; width = 67; height = 22}
let achievements_button_back = {x = 530; y = 170; width = 44; height = 44}
let button_continue = {x = 285; y = 570; width = 151; height = 50}
let tech_button_back = {x = 520; y = 232; width = 44; height = 44}
let tech_button_buy = {x = 367; y = 625; width = 125; height = 30}
let tech_button_navigate = {x = 197; y = 302; width = 38; height = 220}
let buttons_escape = [|escape_resume; escape_settings; escape_credits; escape_exit; clicker_button_settings|]
let buttons_settings = [|settings_button_resume; settings_button_minus; settings_button_plus|]
let buttons_clicker = [|clicker_button_shop; clicker_button_tech; clicker_button_achievements; clicker_button_inventory;
                        clicker_button_settings; clicker_button_event; clicker_entire_area|]
let buttons_shop = [|shop_button1_1; shop_button1_10; shop_button1_100; shop_button1_1000;
                     shop_button2_1; shop_button2_10; shop_button2_100; shop_button2_1000;
                     shop_button3_1; shop_button3_10; shop_button3_100; shop_button3_1000;
                     shop_button_back; clicker_button_shop;|]
let buttons_achievements = [|achievements_button_back; clicker_button_achievements|]
let buttons_level_up = [|button_continue|]
let buttons_tech = [|clicker_button_tech; tech_button_back; tech_button_buy; tech_button_navigate|]
let tech_prices = [|500; 1000; 2500; 5000; 7500;
                    25000; 50000; 100000; 150000; 200000;
                    250000; 300000; 400000; 450000; 500000;|]
let tech_fun = [|(fun game_status -> game_status.pts_per_click <- game_status.pts_per_click +. 2.0);
                 (fun game_status -> game_status.global_mult <- game_status.global_mult *. 2.0);
                 (fun game_status -> game_status.pts_per_sec <- game_status.pts_per_sec +. 1.0);
                 (fun game_status -> game_status.click_mult <- game_status.click_mult *. 1.5);
                 (fun game_status -> game_status.click_count_mult <- 2);
                 (fun game_status -> game_status.pps_mult <- game_status.pps_mult *. 1.5);
                 (fun game_status -> game_status.pts_per_click <- game_status.pts_per_click +. 10.0);
                 (fun game_status -> game_status.global_mult <- game_status.global_mult *. 3.0);
                 (fun game_status -> game_status.pts_per_sec <- game_status.pts_per_sec +. 2.0);
                 (fun game_status -> game_status.click_count_mult <- 3);
                 (fun game_status -> game_status.pts_per_click <- game_status.pts_per_click +. 25.0);
                 (fun game_status -> game_status.pts_per_sec <- game_status.pts_per_sec +. 5.0);
                 (fun game_status -> game_status.global_mult <- game_status.global_mult *. 5.0);
                 (fun game_status -> game_status.click_mult <- game_status.click_mult *. 2.5);
                 (fun game_status -> game_status.click_count_mult <- 4);|]

(* FUNCTIONS *)

let shop_price item (game_status : game_stats) =
  match item with
  | 0 -> 4.0 *. exp (float_of_int game_status.shop_click) +. 50.
  | 1 -> 10.0 *. exp (float_of_int game_status.shop_pps) +. 50.
  | _ -> 40.0 *. exp (float_of_int game_status.shop_mult) +. 50.

let shop_price_iter number item (game_status : game_stats) =
  match item with
  | 0 -> begin
    let price = ref 0.0 in
    for n = game_status.shop_click to game_status.shop_click + number - 1 do
      price := !price +. 4.0 *. exp (float_of_int n) +. 50.
    done;
    !price
  end
  | 1 -> begin
    let price = ref 0.0 in
    for n = game_status.shop_pps to game_status.shop_pps + number - 1 do
      price := !price +. 10.0 *. exp (float_of_int n) +. 50.
    done;
    !price
  end
  | _ -> begin
    let price = ref 0.0 in
    for n = game_status.shop_mult to game_status.shop_mult + number - 1 do
      price := !price +. 40.0 *. exp (float_of_int n) +. 50.
    done;
    !price
  end

let update_requ_exp (game_status : game_stats) =
  let cube x = x*x*x in
  game_status.requ_exp <- float_of_int (8*(cube game_status.level - cube (game_status.level -1)))

let press_button button game_status game_music =
  match game_status.area with
  | Menu -> 
    begin
      if button = main_button_join then
        game_status.area <- Clicker
    end
  | Clicker -> 
    begin
      if button = clicker_button_settings then
        game_status.area <- Escape
      else if button = clicker_button_event then
        begin
          game_status.events_clicked <- game_status.events_clicked +1;
          game_status.click_mult <- game_status.click_mult +. 0.1;
          game_status.event_start <- 0.; (*end the event*)
          clicker_button_event.height <- 0;
          clicker_button_event.width <- 0;
        end
      else if button = clicker_button_shop then
        game_status.area <- Shop
      else if button = clicker_button_achievements then
        game_status.area <- Achievements
      else if button = clicker_button_tech then
        game_status.area <- Tech
      else if button = clicker_button_inventory then
        () (* not developped *)
      else (* not on a button *)
        begin
          let add_of_click = game_status.pts_per_click *. game_status.click_mult *.game_status.global_mult in
          game_status.exp <- game_status.exp +. add_of_click;
          game_status.pts <- game_status.pts +. add_of_click;
          game_status.total_pts <- game_status.total_pts +. add_of_click;
          game_status.clicks_count <- game_status.clicks_count + game_status.click_count_mult;
          if game_status.exp >= game_status.requ_exp then
            begin
              game_status.level <- game_status.level +1;
              game_status.area <- Level_Up;
              Raylib.seek_music_stream game_music.level_up 0.;
              game_status.exp <- game_status.exp -. game_status.requ_exp;
              update_requ_exp game_status;
            end
        end
    end
  | Escape -> 
    begin
      if button = escape_resume || button = clicker_button_settings then
        game_status.area <- Clicker
      else if button = escape_exit then
        Raylib.close_window()
      else if button = escape_credits then 
        game_status.area <- Credits
      else if button = escape_settings then
        game_status.area <- Settings
    end
  | Credits ->
    begin
      if button = credits_button_resume then
        game_status.area <- Escape
    end
  | Settings ->
    begin
      if button = settings_button_resume then
        game_status.area <- Escape
      else if button = settings_button_minus && game_status.music_volume >= 0.1 then
        begin
          game_status.music_volume <- game_status.music_volume -. 0.1;
          Raylib.set_master_volume game_status.music_volume;
        end
      else if button = settings_button_plus && game_status.music_volume < 0.8 then
        begin
          game_status.music_volume <- game_status.music_volume +. 0.1;
          Raylib.set_master_volume game_status.music_volume;
        end
    end
  | Shop ->
    begin
      if button = shop_button_back || button = clicker_button_shop then
        game_status.area <- Clicker
      else
        begin
          let rec ten_power x =
            if x = -1 then 0.1
            else if x = 0 then 1.
            else 10. *. ten_power (x-1)
          in
          for i=0 to 2 do
            for j=0 to 3 do
              if button = buttons_shop.(4*i+j) then
                  begin
                    match i with
                    |0 -> begin
                      let price = shop_price_iter (int_of_float(ten_power j)) i game_status in
                      if game_status.pts >= price then 
                        begin
                        game_status.pts_per_click <-game_status.pts_per_click +. ten_power j; 
                        game_status.shop_click <- game_status.shop_click + int_of_float(ten_power j);
                        game_status.pts <- game_status.pts -. price;
                        end
                    end
                    |1 -> begin
                      let price = shop_price_iter (int_of_float(ten_power j)) i game_status in
                      if game_status.pts >= price then 
                        begin
                          game_status.pts_per_sec <- game_status.pts_per_sec +. ten_power (j-1);
                          game_status.shop_pps <- game_status.shop_pps + int_of_float(ten_power j);
                          game_status.pts <- game_status.pts -. price;
                        end
                    end
                    |_ -> begin
                      let price = shop_price_iter (int_of_float(ten_power j)) i game_status in
                      if game_status.pts >= price then 
                        begin
                          game_status.global_mult <- game_status.global_mult +. ten_power (j-1);
                          game_status.shop_mult <- game_status.shop_mult + int_of_float(ten_power j);
                          game_status.pts <- game_status.pts -. price;
                        end
                    end
                  end
            done
          done
        end
    end
  | Achievements -> 
    begin
      if button = achievements_button_back || button = clicker_button_achievements then
        game_status.area <- Clicker
    end
  | Tech -> 
      begin
        if button = tech_button_back || button = clicker_button_tech then
          game_status.area <- Clicker
        else if button = tech_button_navigate then
          begin
            let t::q = game_status.tech_navigate in
            game_status.tech_position <- t;
            game_status.tech_navigate <- q @ [t];
          end
        else if button = tech_button_buy then
          begin
            if game_status.tech_level.(game_status.tech_position) < 5
            && game_status.pts >= float_of_int tech_prices.(game_status.tech_level.(game_status.tech_position)+5*game_status.tech_position) then
              begin
                game_status.pts <- game_status.pts -. float_of_int tech_prices.(game_status.tech_level.(game_status.tech_position)+5*game_status.tech_position);
                tech_fun.(game_status.tech_level.(game_status.tech_position)+5*game_status.tech_position) game_status;
                game_status.tech_level.(game_status.tech_position) <- game_status.tech_level.(game_status.tech_position) +1;
              end
          end
      end
  | Level_Up -> 
    begin
      game_status.area <- Clicker;
    end
  | _ -> ()

let in_button button x y =
    button.x <= x 
    && x <= button.x + button.width
    && button.y <= y 
    && y <= button.y + button.height

(*_____________________________________________*)

(*  
 __    __  __    __  _______  
|  \  |  \|  \  |  \|       \ 
| $$  | $$| $$  | $$| $$$$$$$\
| $$__| $$| $$  | $$| $$  | $$
| $$    $$| $$  | $$| $$  | $$
| $$$$$$$$| $$  | $$| $$  | $$
| $$  | $$| $$__/ $$| $$__/ $$
| $$  | $$ \$$    $$| $$    $$
 \$$   \$$  \$$$$$$  \$$$$$$$ 
*)

(* FUNCTIONS *)

let update_health game_status game_textures =
  if (game_status.life /. game_status.max_life *. 100.0) >= 2.0 then
    begin
      let open Raylib in
      draw_texture game_textures.life_progress0 165 9 Color.white;
      let temp = int_of_float(game_status.life /. game_status.max_life *. 100.0 -. 2.0) / 2 in
      for i = 0 to temp do
        draw_texture game_textures.life_progress (167 + i * 6) 9 Color.white;
      done;
      if temp > 0 then
        draw_texture game_textures.life_progress1 (167 + temp * 6) 9 Color.white;
    end

let update_exp game_status game_textures =
  if (game_status.exp /. game_status.requ_exp *. 100.0) >= 2.0 then
    begin
      let open Raylib in
      draw_texture game_textures.xp_progress0 165 49 Color.white;
      let temp = int_of_float(game_status.exp /. game_status.requ_exp *. 100.0 -. 2.0) / 2 - 1 in
      for i = 0 to temp do
        draw_texture game_textures.xp_progress (167 + i * 6) 49 Color.white
      done;
      if temp > 0 then
        draw_texture game_textures.xp_progress1 (167 + temp * 6) 49 Color.white
    end

let update_HUD game_status game_textures =
  update_health game_status game_textures;
  update_exp game_status game_textures


let draw_HUD game_textures =
  let open Raylib in
  draw_texture game_textures.avatar_background 5 5 Color.white;
  draw_texture game_textures.character 23 25 Color.white;
  draw_texture game_textures.life_bar 125 5 Color.white;
  draw_texture game_textures.xp_bar 125 45 Color.white;
  draw_texture game_textures.coins_bar 125 85 Color.white


(*_____________________________________________*)

(* 
  ______    ______   __    __  ______  ________  __     __  ________  
 /      \  /      \ |  \  |  \|      \|        \|  \   |  \|        \
|  $$$$$$\|  $$$$$$\| $$  | $$ \$$$$$$| $$$$$$$$| $$   | $$| $$$$$$$$
| $$__| $$| $$   \$$| $$__| $$  | $$  | $$__    | $$   | $$| $$__    
| $$    $$| $$      | $$    $$  | $$  | $$  \    \$$\ /  $$| $$  \   
| $$$$$$$$| $$   __ | $$$$$$$$  | $$  | $$$$$     \$$\  $$ | $$$$$   
| $$  | $$| $$__/  \| $$  | $$ _| $$_ | $$_____    \$$ $$  | $$_____ 
| $$  | $$ \$$    $$| $$  | $$|   $$ \| $$     \    \$$$   | $$     \
 \$$   \$$  \$$$$$$  \$$   \$$ \$$$$$$ \$$$$$$$$     \$     \$$$$$$$$
*)

(* TYPES *)

type achievement = {
  id : int;
  name : string;
  task : success;
  amount : int;
  next : int;
  mutable status : bool
}

let achievement_1 = { id = 1; name = "Clicker Novice"; task = Click; amount = 50; next = 2; status = false }
let achievement_2 = { id = 2; name = "Clicker Débutant"; task = Click; amount = 250; next = 3; status = false }
let achievement_3 = { id = 3; name = "Clicker Néophyte"; task = Click; amount = 1000; next = 4; status = false }
let achievement_4 = { id = 4; name = "Clicker Professionnel"; task = Click; amount = 5000; next = 5; status = false }
let achievement_5 = { id = 5; name = "Clicker Expert"; task = Click; amount = 25000; next = 6; status = false }
let achievement_6 = { id = 6; name = "Maître Clicker !"; task = Click; amount = 100000; next = 0; status = false }
let achievement_7 = { id = 7; name = "Premières Pièces"; task = Points; amount = 10; next = 8; status = false }
let achievement_8 = { id = 8; name = "Petite Richesse"; task = Points; amount = 1000; next = 9; status = false }
let achievement_9 = { id = 9; name = "Petit Magot"; task = Points; amount = 5000; next = 10; status = false }
let achievement_10 = { id = 10; name = "Grand Magot"; task = Points; amount = 100000; next = 11; status = false }
let achievement_11 = { id = 11; name = "Presque Riche"; task = Points; amount = 500000; next = 12; status = false }
let achievement_12 = { id = 12; name = "Richissime !"; task = Points; amount = 1000000; next = 0; status = false }
let achievement_13 = { id = 13; name = "1 Point par Seconde"; task = PPS; amount = 1; next = 14; status = false }
let achievement_14 = { id = 14; name = "5 Points par Seconde"; task = PPS; amount = 5; next = 15; status = false }
let achievement_15 = { id = 15; name = "25 Points par Seconde"; task = PPS; amount = 25; next = 16; status = false }
let achievement_16 = { id = 16; name = "100 Points par Seconde"; task = PPS; amount = 100; next = 17; status = false }
let achievement_17 = { id = 17; name = "1000 Points par Seconde"; task = PPS; amount = 1000; next = 18; status = false }
let achievement_18 = { id = 18; name = "Plus Besoin de Cliquer !"; task = PPS; amount = 10000; next = 0; status = false }
let achievement_19 = { id = 19; name = "Premier Bonus"; task = Events; amount = 1; next = 20; status = false }
let achievement_20 = { id = 20; name = "Quelques Bonus"; task = Events; amount = 5; next = 21; status = false }
let achievement_21 = { id = 21; name = "10 Bonus"; task = Events; amount = 10; next = 22; status = false }
let achievement_22 = { id = 22; name = "Plein de Bonus"; task = Events; amount = 25; next = 23; status = false }
let achievement_23 = { id = 23; name = "Trop de Bonus"; task = Events; amount = 50; next = 24; status = false }
let achievement_24 = { id = 24; name = "Bonus Grinder !"; task = Events; amount = 100; next = 0; status = false }
let achievement_25 = { id = 25; name = "4% du Jeu"; task = Achievements; amount = 1; next = 26; status = false }
let achievement_26 = { id = 26; name = "20% du Jeu"; task = Achievements; amount = 6; next = 27; status = false }
let achievement_27 = { id = 27; name = "40% du Jeu"; task = Achievements; amount = 12; next = 28; status = false }
let achievement_28 = { id = 28; name = "60% du Jeu"; task = Achievements; amount = 18; next = 29; status = false }
let achievement_29 = { id = 29; name = "80% du Jeu"; task = Achievements; amount = 24; next = 30; status = false }
let achievement_30 = { id = 30; name = "100% !"; task = Achievements; amount = 29; next = 0; status = false }
let achievements = [|achievement_1; achievement_2; achievement_3; achievement_4; achievement_5; achievement_6;
achievement_7; achievement_8; achievement_9; achievement_10; achievement_11; achievement_12;
achievement_13; achievement_14; achievement_15; achievement_16; achievement_17; achievement_18;
achievement_19; achievement_20; achievement_21; achievement_22; achievement_23; achievement_24;
achievement_25; achievement_26; achievement_27; achievement_28; achievement_29; achievement_30|]
let update_achievements game_status =
  for i = 0 to Array.length achievements - 1 do
    if not(achievements.(i).status) then
      begin
      if achievements.(i).task = Click then
        begin
        if achievements.(i).amount <= game_status.clicks_count then
          begin
            achievements.(i).status <- true;
            game_status.achievements_count <- game_status.achievements_count + 1;
            game_status.success_display_start <- Raylib.get_time();
            game_status.last_success <- Click;
          end
        end
      else if achievements.(i).task = Points then
        begin
        if achievements.(i).amount <= int_of_float(game_status.total_pts) then
          begin
            achievements.(i).status <- true;
            game_status.achievements_count <- game_status.achievements_count + 1;
            game_status.success_display_start <- Raylib.get_time();
            game_status.last_success <- Points;
          end
        end
      else if achievements.(i).task = PPS then
        begin
          if achievements.(i).amount <= int_of_float(game_status.pts_per_sec *. game_status.pps_mult) then
          begin
            achievements.(i).status <- true;
            game_status.achievements_count <- game_status.achievements_count + 1;
            game_status.success_display_start <- Raylib.get_time();
            game_status.last_success <- PPS;
          end
        end
      else if achievements.(i).task = Achievements then
        begin
        if achievements.(i).amount <= game_status.achievements_count then
          begin
            achievements.(i).status <- true;
            game_status.achievements_count <- game_status.achievements_count + 1;
          end
        end
      else if achievements.(i).task = Events then
        begin
        if achievements.(i).amount <= game_status.events_clicked then
          begin
            achievements.(i).status <- true;
            game_status.achievements_count <- game_status.achievements_count + 1;
            game_status.success_display_start <- Raylib.get_time();
            game_status.last_success <- Events;
          end
        end
      end
    done


(*_____________________________________________*)

(*
 _______   __         ______  __      __  ______  __    __   ______  
|       \ |  \       /      \|  \    /  \|      \|  \  |  \ /      \ 
| $$$$$$$\| $$      |  $$$$$$\\$$\  /  $$ \$$$$$$| $$\ | $$|  $$$$$$\
| $$__/ $$| $$      | $$__| $$ \$$\/  $$   | $$  | $$$\| $$| $$ __\$$
| $$    $$| $$      | $$    $$  \$$  $$    | $$  | $$$$\ $$| $$|    \
| $$$$$$$ | $$      | $$$$$$$$   \$$$$     | $$  | $$\$$ $$| $$ \$$$$
| $$      | $$_____ | $$  | $$   | $$     _| $$_ | $$ \$$$$| $$__| $$
| $$      | $$     \| $$  | $$   | $$    |   $$ \| $$  \$$$ \$$    $$
 \$$       \$$$$$$$$ \$$   \$$    \$$     \$$$$$$ \$$   \$$  \$$$$$$ 
*)

(* FUNCTIONS *)

let event_button game_status =
  if game_status.event_start <> 0. && Raylib.get_time() -. game_status.event_start >= 5. then (*during an event*)
    begin
      game_status.event_start <- 0.;
      clicker_button_event.height <- 0;
      clicker_button_event.width <- 0;
    end
  else if game_status.event_start = 0. && Random.int 100 = 1 then (* 1 chance out of 100 *)
      begin
        game_status.event_start <- Raylib.get_time();
        clicker_button_event.x <- Random.int (width-75);
        clicker_button_event.y <- 200 + Random.int 500;
        clicker_button_event.height <- 75;
        clicker_button_event.width <- 75;
      end

let click_area buttons_area x y game_status game_music=
  let on_button = ref false in
  for i = 0 to Array.length buttons_area -1 do
    if in_button buttons_area.(i) x y && not !on_button then (* only one button at a time *)
      begin
        press_button buttons_area.(i) game_status game_music;
        on_button := true;
      end
  done

let draw_pts game_status =
  let text = string_of_int (int_of_float(floor game_status.pts)) in
  let i = ref 35 in
  while 190 <= Raylib.measure_text text !i  do
    decr i
  done;
  Raylib.draw_text text (163 + 233/2 - (Raylib.measure_text text !i) / 2) (106 - !i/2) !i Raylib.Color.darkbrown

let draw_game_area (game_status : game_stats) (game_textures : game_textures) : unit = 
  let open Raylib in
  match game_status.area with
  | Escape -> 
    begin
      begin_drawing ();
      draw_texture game_textures.escape 105 115 Color.white;
      end_drawing();
    end
  | Credits -> 
    begin
      begin_drawing ();
      draw_texture game_textures.credits_menu 105 115 Color.white;
      end_drawing();
    end  
  | Settings -> 
    begin
      begin_drawing ();
      draw_texture game_textures.settings_menu 105 115 Color.white;
      draw_text (string_of_int (int_of_float (100. *. game_status.music_volume))) 487 358 30 Color.black;
      end_drawing();
    end
  | Menu -> 
    begin 
      set_window_size 560 250; 
      begin_drawing ();
      clear_background Color.raywhite;
      draw_texture game_textures.background 0 0 Color.white;
      draw_texture game_textures.main_title 40 40 Color.white;
      draw_texture game_textures.join 172 155 Color.white;
      end_drawing();
    end
  | Clicker -> 
    begin
      set_window_size width height; 
      begin_drawing ();
      clear_background Color.raywhite;
      draw_texture game_textures.background 0 0 Color.white;
      let xp_progression = game_status.exp /. game_status.requ_exp *. 100.0 in
      if xp_progression <= 18.0 then
        draw_texture game_textures.explo1 160 210 Color.white
      else if xp_progression <= 34.0 then
        draw_texture game_textures.explo2 160 210 Color.white
      else if xp_progression <= 50.0 then
        draw_texture game_textures.explo3 160 210 Color.white
      else if xp_progression <= 68.0 then
        draw_texture game_textures.explo4 160 210 Color.white
      else if xp_progression <= 84.0 then
        draw_texture game_textures.explo5 160 210 Color.white
      else 
        draw_texture game_textures.explo6 160 210 Color.white;
      draw_texture game_textures.button_shop 5 834 Color.white;
      draw_texture game_textures.button_inventory 185 842 Color.white;
      draw_texture game_textures.button_tech 365 846 Color.white;
      draw_texture game_textures.button_achievements 545 839 Color.white;
      draw_HUD game_textures;
      draw_pts game_status;
      draw_texture game_textures.button_settings 675 5 Color.white;
      if game_status.event_start <> 0. then
        draw_texture game_textures.bonus (clicker_button_event.x) (clicker_button_event.y) Color.white;
      if game_status.success_display_start <> 0. then
        begin
          draw_texture game_textures.success_display 211 740 Color.white;
          match game_status.last_success with
          |Points -> draw_texture game_textures.success_points 230 755 Color.white
          |PPS -> draw_texture game_textures.success_pps 230 755 Color.white
          |Events -> draw_texture game_textures.success_event 230 755 Color.white
          |_ -> ()
        end;
      update_HUD game_status game_textures;
      end_drawing();
    end
  | Shop -> 
    begin
      begin_drawing();
      draw_texture game_textures.shop_menu 156 274 Color.white;
      draw_texture game_textures.button_back 520 274 Color.white;
      draw_HUD game_textures;
      draw_pts game_status;
      let texts1 = [|"Ajouter 1 Point par Click"; "Ajouter 0.1 Point par Seconde"; "Ajouter x0.1 au Multplicateur"|] in
      let texts3 = [|game_status.pts_per_click; game_status.pts_per_sec; game_status.global_mult|] in
      for i=0 to 2 do
        draw_text texts1.(i) 277 (356+95*i) 17 Color.black;
        let text2 = Printf.sprintf "%.2f /u" (shop_price i game_status) in
        let font_size2 = ref 18 in
        while measure_text text2 !font_size2 >= 103 && !font_size2 > 0 do
          decr font_size2
        done;
        draw_text text2 (470- (measure_text text2 !font_size2)/2) (383+95*i) !font_size2 Color.black;
        let text3 = Printf.sprintf "%.2f" texts3.(i) in
        let font_size3 = ref 18 in
        while measure_text text3 !font_size3 >= 45 && !font_size3 > 0 do
          decr font_size3
        done;
        draw_text text3 (233 -(measure_text text3 !font_size3)/2) (413+95*i) !font_size3 Color.black;
      done;
      update_HUD game_status game_textures;
      end_drawing();
    end
  | Achievements ->
    begin
      begin_drawing();
      draw_texture game_textures.achievements_menu 156 173 Color.white;
      draw_texture game_textures.button_back 530 170 Color.white;
      for i=0 to 4 do
        let j = ref (5+i*6) in
        if not achievements.(!j).status then (*if he has not achieved everything in the category*)
          begin
            while !j>6*i && not achievements.(!j-1).status do
              decr j;
            done;
          end;
        draw_text achievements.(!j).name 280 (260+95*i) 19 Color.black;
        let text = ref "" in
        if i=0 then
          text := Printf.sprintf "%i / %i" game_status.clicks_count achievements.(!j).amount
        else if i=1 then
          text := Printf.sprintf "%i / %i" (int_of_float game_status.total_pts) achievements.(!j).amount
        else if i=2 then
          text := Printf.sprintf "%i / %i" (int_of_float(game_status.pts_per_sec *. game_status.pps_mult)) achievements.(!j).amount
        else if i=3 then
          text := Printf.sprintf "%i / %i" game_status.events_clicked achievements.(!j).amount
        else if i=4 then
          text := Printf.sprintf "%i / %i" game_status.achievements_count achievements.(!j).amount;
        let font_size = ref 23 in
        while measure_text !text !font_size >= 120 && !font_size > 0 do
          decr font_size;
        done;
        draw_text !text (448 - (measure_text !text !font_size)/2) (297+95*i) !font_size Color.black
      done;
      end_drawing();
    end
  | Tech -> 
    begin
      begin_drawing();
      draw_HUD game_textures;
      draw_pts game_status;
      draw_texture game_textures.tech_buy_sign 208 535 Color.white;
      begin
        match game_status.tech_position with
        |0 -> draw_texture game_textures.tech1 156 232 Color.white
        |1 -> draw_texture game_textures.tech2 156 232 Color.white
        |_ -> draw_texture game_textures.tech3 156 232 Color.white
      end;
      draw_texture game_textures.button_back 520 232 Color.white;
      let icons_positions = [|(344,306); (416,342); (452,414); (380,486); (308,414);
                              (452,378); (380,306); (308,378); (416,450); (344,486);
                              (488,306); (380,342); (452,414); (380,486); (308,414)|] in
      let texts = [|"2 Points par Clic"; "2 fois plus de Points"; "1 Point par Seconde"; "1.5x Points par Clic"; "Clics comptent Doubles";
                    "1.5x Points par Seconde"; "10 Points par Clic"; "3 fois plus de Points"; "2 Points par Seconde"; "Clics comptent Triples";
                    "25 Points par Clic"; "5 Points par Seconde"; "5 fois plus de Points"; "2.5x Points par Clic"; "Clics comptent Quadruples"
                   |] in
      if game_status.tech_level.(game_status.tech_position) < 5 then (*not everything unlocked in the category*)
        begin
          if not ((game_status.tech_position, game_status.tech_level.(game_status.tech_position)) = (0, 0)) then (*it would be an overwritten*)
            draw_texture game_textures.tech_advancement.(game_status.tech_level.(game_status.tech_position)+5*game_status.tech_position) 230 571 Color.white;
          for i=(game_status.tech_level.(game_status.tech_position)+5*game_status.tech_position) to (4+5*game_status.tech_position) do
            let x,y = icons_positions.(i) in
            draw_texture game_textures.tech_locked x y Color.white;
          done;
          let text1 = texts.(game_status.tech_level.(game_status.tech_position)+5*game_status.tech_position) in
          let font_size = ref 30 in
          while measure_text text1 !font_size >= 198 do
            decr font_size;
          done;
          draw_text text1 (388-(measure_text text1 !font_size)/2) 587 !font_size Color.black;
          let text2 =  string_of_int tech_prices.(game_status.tech_level.(game_status.tech_position)+5*game_status.tech_position) in
          font_size := 30;
          while measure_text text2 !font_size >= 115 do
            decr font_size;
          done;
          draw_text text2 (294-(measure_text text2 !font_size)/2) 628 !font_size Color.black;
        end
      else (*draw the last unlocked tech in the category*)
        begin
          draw_texture game_textures.tech_advancement.(4+5*game_status.tech_position) 232 572 Color.white;
          let text1 = texts.(game_status.tech_level.(game_status.tech_position)+5*game_status.tech_position -1) in
          let font_size = ref 30 in
          while measure_text text1 !font_size >= 198 do
            decr font_size;
          done;
          draw_text text1 (388-(measure_text text1 !font_size)/2) 587 !font_size Color.black;
          draw_text "Obtenu" 254 630 25 Color.black;
        end;
      update_HUD game_status game_textures;
      end_drawing();
    end
  | Level_Up -> 
    begin
      begin_drawing();
      draw_texture game_textures.level_up_card 250 301 Color.white;
      draw_text (string_of_int (game_status.level -1)) (299- (measure_text (string_of_int (game_status.level -1)) 40)/2) 502 40 Color.darkbrown;
      draw_text (string_of_int game_status.level) (420 - (measure_text (string_of_int game_status.level) 40)/2) 502 40 Color.darkbrown;
      draw_rectangle_lines 274 490 50 57 Color.darkbrown;
      draw_rectangle_lines 395 490 50 57 Color.darkbrown;
      end_drawing();
    end
  | _ -> ()

(*_____________________________________________*)

(*
 ______  __    __  ______  ________ 
|      \|  \  |  \|      \|        \
 \$$$$$$| $$\ | $$ \$$$$$$ \$$$$$$$$
  | $$  | $$$\| $$  | $$     | $$   
  | $$  | $$$$\ $$  | $$     | $$   
  | $$  | $$\$$ $$  | $$     | $$   
 _| $$_ | $$ \$$$$ _| $$_    | $$   
|   $$ \| $$  \$$$|   $$ \   | $$   
 \$$$$$$ \$$   \$$ \$$$$$$    \$$   
*)

let game_init () : game_stats = 
  {
    pts_per_click = 1.0;
    pts_per_sec = 0.0;
    pts = 0.0;
    clicks_count = 0;
    total_pts = 0.0;
    prestige = 0;
    global_mult = 1.0;
    click_mult = 1.0;
    pps_mult = 1.0;
    click_count_mult = 1;
    achievements_count = 0;
    time_spent = 0;
    area = Menu;
    level = 1;
    exp = 0.0;
    requ_exp = 10.0;
    life = 100.0;
    max_life = 100.0;
    defence = 0.0;
    attack = 1.0;
    event_start = 0.0;
    events_clicked = 0;
    success_display_start = 0.0;
    last_success = Click;
    tech_navigate = [0; 1; 2];
    tech_position = 0;
    tech_level = [|0; 0; 0|];
    shop_click = 0;
    shop_pps = 0;
    shop_mult = 0;
    music_volume = 0.5;
  }

let game_assets_init () : game_assets = 
  {
    background = Raylib.load_image "assets/background.png";
    character = Raylib.load_image "assets/character.png";
    coins_bar = Raylib.load_image "assets/coins_bar.png";
    avatar_background = Raylib.load_image "assets/avatar_background.png";
    coins = Raylib.load_image "assets/coins.png";
    heart = Raylib.load_image "assets/heart.png";
    join = Raylib.load_image "assets/join.png";
    level = Raylib.load_image "assets/level.png";
    life_bar = Raylib.load_image "assets/life_bar.png";
    life_progress = Raylib.load_image "assets/life_progress.png";
    life_progress0 = Raylib.load_image "assets/life_progress0.png";
    life_progress1 = Raylib.load_image "assets/life_progress1.png";
    main_title = Raylib.load_image "assets/main_title.png";
    xp_bar = Raylib.load_image "assets/xp_bar.png";
    escape = Raylib.load_image "assets/escape.png";
    xp_progress = Raylib.load_image "assets/xp_progress.png";
    xp_progress0 = Raylib.load_image "assets/xp_progress0.png";
    xp_progress1 = Raylib.load_image "assets/xp_progress1.png";
    button_settings = Raylib.load_image "assets/button_settings.png";
    explo1 = Raylib.load_image "assets/explo1.png";
    explo2 = Raylib.load_image "assets/explo2.png";
    explo3 = Raylib.load_image "assets/explo3.png";
    explo4 = Raylib.load_image "assets/explo4.png";
    explo5 = Raylib.load_image "assets/explo5.png";
    explo6 = Raylib.load_image "assets/explo6.png";
    button_inventory = Raylib.load_image "assets/button_inventory.png";
    button_achievements = Raylib.load_image "assets/button_achievements.png";
    button_tech = Raylib.load_image "assets/button_tech.png";
    button_shop = Raylib.load_image "assets/button_shop.png";
    achievements_menu = Raylib.load_image "assets/achievements_menu.png";
    button_back = Raylib.load_image "assets/button_back.png";
    bonus = Raylib.load_image "assets/bonus.png";
    shop_menu = Raylib.load_image "assets/shop_menu.png";
    success_display = Raylib.load_image "assets/success_display.png";
    success_points = Raylib.load_image "assets/success_points.png";
    success_pps = Raylib.load_image "assets/success_pps.png";
    success_event = Raylib.load_image "assets/success_event.png";
    level_up_card = Raylib.load_image "assets/level_up_card.png";
    tech1 = Raylib.load_image "assets/tech1.png";
    tech1_2 = Raylib.load_image "assets/tech1_2.png";
    tech1_3 = Raylib.load_image "assets/tech1_3.png";
    tech1_4 = Raylib.load_image "assets/tech1_4.png";
    tech1_5 = Raylib.load_image "assets/tech1_5.png";
    tech2 = Raylib.load_image "assets/tech2.png";
    tech2_1 = Raylib.load_image "assets/tech2_1.png";
    tech2_2 = Raylib.load_image "assets/tech2_2.png";
    tech2_3 = Raylib.load_image "assets/tech2_3.png";
    tech2_4 = Raylib.load_image "assets/tech2_4.png";
    tech2_5 = Raylib.load_image "assets/tech2_5.png";
    tech3 = Raylib.load_image "assets/tech3.png";
    tech3_1 = Raylib.load_image "assets/tech3_1.png";
    tech3_2 = Raylib.load_image "assets/tech3_2.png";
    tech3_3 = Raylib.load_image "assets/tech3_3.png";
    tech3_4 = Raylib.load_image "assets/tech3_4.png";
    tech3_5 = Raylib.load_image "assets/tech3_5.png";
    tech_buy_sign = Raylib.load_image "assets/tech_buy_sign.png";
    tech_locked = Raylib.load_image "assets/tech_locked.png";
    credits_menu = Raylib.load_image "assets/credits_menu.png";
    settings_menu = Raylib.load_image "assets/settings_menu.png";
  }
let game_textures_init (game_assets : game_assets) : game_textures = 
  {
    background = Raylib.load_texture_from_image game_assets.background;
    character = Raylib.load_texture_from_image game_assets.character;
    coins_bar = Raylib.load_texture_from_image game_assets.coins_bar;
    avatar_background = Raylib.load_texture_from_image game_assets.avatar_background;
    coins = Raylib.load_texture_from_image game_assets.coins;
    heart = Raylib.load_texture_from_image game_assets.heart;
    join = Raylib.load_texture_from_image game_assets.join;
    level = Raylib.load_texture_from_image game_assets.level;
    life_bar = Raylib.load_texture_from_image game_assets.life_bar;
    life_progress = Raylib.load_texture_from_image game_assets.life_progress;
    life_progress0 = Raylib.load_texture_from_image game_assets.life_progress0;
    life_progress1 = Raylib.load_texture_from_image game_assets.life_progress1;
    main_title = Raylib.load_texture_from_image game_assets.main_title;
    xp_bar = Raylib.load_texture_from_image game_assets.xp_bar;
    escape = Raylib.load_texture_from_image game_assets.escape;
    xp_progress = Raylib.load_texture_from_image game_assets.xp_progress;
    xp_progress0 = Raylib.load_texture_from_image game_assets.xp_progress0;
    xp_progress1 = Raylib.load_texture_from_image game_assets.xp_progress1;
    button_settings = Raylib.load_texture_from_image game_assets.button_settings;
    explo1 = Raylib.load_texture_from_image game_assets.explo1;
    explo2 = Raylib.load_texture_from_image game_assets.explo2;
    explo3 = Raylib.load_texture_from_image game_assets.explo3;
    explo4 = Raylib.load_texture_from_image game_assets.explo4;
    explo5 = Raylib.load_texture_from_image game_assets.explo5;
    explo6 = Raylib.load_texture_from_image game_assets.explo6;
    button_inventory = Raylib.load_texture_from_image game_assets.button_inventory;
    button_achievements = Raylib.load_texture_from_image game_assets.button_achievements;
    button_tech = Raylib.load_texture_from_image game_assets.button_tech;
    button_shop = Raylib.load_texture_from_image game_assets.button_shop;
    achievements_menu = Raylib.load_texture_from_image game_assets.achievements_menu;
    button_back = Raylib.load_texture_from_image game_assets.button_back;
    bonus = Raylib.load_texture_from_image game_assets.bonus;
    shop_menu = Raylib.load_texture_from_image game_assets.shop_menu;
    success_display = Raylib.load_texture_from_image game_assets.success_display;
    success_points = Raylib.load_texture_from_image game_assets.success_points;
    success_pps = Raylib.load_texture_from_image game_assets.success_pps;
    success_event = Raylib.load_texture_from_image game_assets.success_event;
    level_up_card = Raylib.load_texture_from_image game_assets.level_up_card;
    tech1 = Raylib.load_texture_from_image game_assets.tech1;
    tech2 = Raylib.load_texture_from_image game_assets.tech2;
    tech3 = Raylib.load_texture_from_image game_assets.tech3;
    tech_buy_sign = Raylib.load_texture_from_image game_assets.tech_buy_sign;
    tech_locked = Raylib.load_texture_from_image game_assets.tech_locked;
    tech_advancement = [|
    Raylib.load_texture_from_image game_assets.tech_buy_sign;
    Raylib.load_texture_from_image game_assets.tech1_2;
    Raylib.load_texture_from_image game_assets.tech1_3;
    Raylib.load_texture_from_image game_assets.tech1_4;
    Raylib.load_texture_from_image game_assets.tech1_5;
    Raylib.load_texture_from_image game_assets.tech2_1;
    Raylib.load_texture_from_image game_assets.tech2_2;
    Raylib.load_texture_from_image game_assets.tech2_3;
    Raylib.load_texture_from_image game_assets.tech2_4;
    Raylib.load_texture_from_image game_assets.tech2_5;
    Raylib.load_texture_from_image game_assets.tech3_1;
    Raylib.load_texture_from_image game_assets.tech3_2;
    Raylib.load_texture_from_image game_assets.tech3_3;
    Raylib.load_texture_from_image game_assets.tech3_4;
    Raylib.load_texture_from_image game_assets.tech3_5|];
    credits_menu = Raylib.load_texture_from_image game_assets.credits_menu;
    settings_menu = Raylib.load_texture_from_image game_assets.settings_menu;
  }

let game_music_init () : game_music = 
  {
    background = Raylib.load_music_stream "musics/background.mp3";
    level_up = Raylib.load_music_stream "musics/level_up.mp3";
  }

(*_____________________________________________*)

(*
  ______    ______   _______   ________ 
 /      \  /      \ |       \ |        \
|  $$$$$$\|  $$$$$$\| $$$$$$$\| $$$$$$$$
| $$   \$$| $$  | $$| $$__| $$| $$__    
| $$      | $$  | $$| $$    $$| $$  \   
| $$   __ | $$  | $$| $$$$$$$\| $$$$$   
| $$__/  \| $$__/ $$| $$  | $$| $$_____ 
 \$$    $$ \$$    $$| $$  | $$| $$     \
  \$$$$$$   \$$$$$$  \$$   \$$ \$$$$$$$$
*)

(* FUNCTIONS *)

let rec loop (game_status : game_stats) (game_textures : game_textures) (game_music : game_music) =
  match Raylib.window_should_close () with
  | true -> 
    begin
      if game_status.area <> Menu then
        begin
          game_status.area <- Escape;
          draw_game_area game_status game_textures;
          if Raylib.is_key_pressed Raylib.Key.Escape then
            game_status.area <- Clicker
          else if Raylib.is_key_pressed Raylib.Key.Enter then
            Raylib.close_window()
          else if Raylib.is_mouse_button_pressed Raylib.MouseButton.Left then
            click_area buttons_escape (Raylib.get_mouse_x()) (Raylib.get_mouse_y()) game_status game_music;
        end
      else Raylib.close_window ();
      Raylib.update_music_stream game_music.background;
      loop game_status game_textures game_music;
    end
  | false -> 
    begin
      draw_game_area game_status game_textures;
      if game_status.area = Menu then
        begin
          if Raylib.is_mouse_button_pressed Raylib.MouseButton.Left then 
            if in_button main_button_join (Raylib.get_mouse_x ()) (Raylib.get_mouse_y ()) then
              press_button main_button_join game_status game_music;
        end
      else if game_status.area = Clicker then
        begin
          event_button game_status;
          game_status.pts <- game_status.pts +. game_status.pts_per_sec *. game_status.pps_mult *. game_status.global_mult *. Raylib.get_frame_time (); 
          game_status.total_pts <- game_status.total_pts +. game_status.pts_per_sec *. game_status.pps_mult *. game_status.global_mult *. Raylib.get_frame_time () ;
          if game_status.success_display_start <> 0. && Raylib.get_time() -. game_status.success_display_start >= 5. then
            game_status.success_display_start <- 0.
          else if Raylib.is_mouse_button_pressed Raylib.MouseButton.Left then 
            click_area buttons_clicker (Raylib.get_mouse_x()) (Raylib.get_mouse_y()) game_status game_music
        end
      else if game_status.area = Escape then
        begin
          if Raylib.is_key_pressed Raylib.Key.Enter then
            Raylib.close_window()
          else if Raylib.is_key_down Raylib.Key.Escape then
            game_status.area <- Clicker
          else if Raylib.is_mouse_button_pressed Raylib.MouseButton.Left then
            click_area buttons_escape (Raylib.get_mouse_x()) (Raylib.get_mouse_y()) game_status game_music;
        end
      else if game_status.area = Credits then
        begin
          if (Raylib.is_mouse_button_pressed Raylib.MouseButton.Left) && (in_button credits_button_resume (Raylib.get_mouse_x ()) (Raylib.get_mouse_y ())) then
            press_button credits_button_resume game_status game_music
        end
      else if game_status.area = Settings then
        begin
          if (Raylib.is_mouse_button_pressed Raylib.MouseButton.Left) then
            click_area buttons_settings (Raylib.get_mouse_x()) (Raylib.get_mouse_y())  game_status game_music
        end
      else if game_status.area = Shop then
        begin
          if Raylib.is_mouse_button_pressed Raylib.MouseButton.Left then 
            click_area buttons_shop (Raylib.get_mouse_x()) (Raylib.get_mouse_y()) game_status game_music;
        end
      else if game_status.area = Achievements then
        begin
          if Raylib.is_mouse_button_pressed Raylib.MouseButton.Left then 
            click_area buttons_achievements (Raylib.get_mouse_x()) (Raylib.get_mouse_y()) game_status game_music;
        end
      else if game_status.area = Tech then
        begin
          if Raylib.is_mouse_button_pressed Raylib.MouseButton.Left then 
            click_area buttons_tech (Raylib.get_mouse_x()) (Raylib.get_mouse_y()) game_status game_music;
        end
      else if game_status.area = Level_Up then
        begin
          if not (Raylib.get_music_time_played game_music.level_up >= Raylib.get_music_time_length game_music.level_up -. 0.1) then
            Raylib.update_music_stream game_music.level_up;
          if Raylib.is_mouse_button_pressed Raylib.MouseButton.Left then 
            click_area buttons_level_up (Raylib.get_mouse_x()) (Raylib.get_mouse_y()) game_status game_music;
        end;
      Raylib.update_music_stream game_music.background;
      update_achievements game_status;
      loop game_status game_textures game_music
    end

let () =
  setup ();
  let game_status = game_init () in
  let game_assets = game_assets_init () in
  let game_textures = game_textures_init game_assets in
  let game_music = game_music_init () in
  Raylib.play_music_stream game_music.background;
  Raylib.play_music_stream game_music.level_up;
  loop game_status game_textures game_music